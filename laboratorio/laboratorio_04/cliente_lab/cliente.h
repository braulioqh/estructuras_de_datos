#ifndef CLIENTE_H
#define CLIENTE_H
#include <string>

typedef struct{
  int dia;
  int mes;
  int anho;
}fecha_t;

typedef struct{
  std::string calle;
  int numero;
  int cod_postal;
  std::string poblacion;
}direccion_t;

typedef struct{
  std::string casa;
  std::string oficina;
  std::string movil;
}telefono_t;

class Cliente{
  //Atributos (variables)
private:
  std::string nombre;
  std::string apellido_paterno;
  std::string apellido_materno;
  fecha_t fechaNacimiento;
  direccion_t domicilio;
  telefono_t contacto;
  //Métodos (funciones)
public:
  void set_nombre(std::string nombre);
  std::string get_nombre();
  void set_fechaNacimiento(fecha_t );
  void set_fechaNacimiento(int , int , int );
  fecha_t get_fechaNacimiento();
};
#endif
