#include "cliente.h"

cliente::cliente (std::string,std::string,std::string,fecha_t, direccion_t,telefono_t ){

}
cliente::cliente (){

}
void cliente::set_nombre(const std::string nombre){
  this->nombre=nombre;
}
void cliente::set_apellido_paterno(std::string apellido){
  this->apellido_paterno=apellido;
}
void cliente::set_apellido_materno(std::string apellido){
  this->apellido_materno=apellido;
}
void cliente::set_fechaNacimiento(int dia, int mes, int anho){
  this->fechaNacimiento.dia= dia;
  this->fechaNacimiento.mes= mes;
  this->fechaNacimiento.anho= anho;
}
void cliente::set_fechaNacimiento(fecha_t fechaNacimiento){
  this->fechaNacimiento = fechaNacimiento;
}
void cliente::set_domicilio(std::string calle, int numero, int cod_postal,
   std::string poblacion ){
     this->domicilio.calle = calle;
     this->domicilio.numero = numero;
     this->domicilio.cod_postal = cod_postal;
     this->domicilio.poblacion = poblacion;
}
void cliente::set_contacto(std::string casa, std::string oficina, std::string movil){
  this->contacto.casa = casa;
  this->contacto.oficina = oficina;
  this->contacto.movil = movil;
}
std::string cliente::get_nombre(){
  return this->nombre;
}
std::string cliente::get_apellido_paterno(){
  return this->apellido_paterno;
}
std::string cliente::get_apellido_materno(){
  return this->apellido_materno;
}
fecha_t cliente::get_fechaNacimiento(){
  return this->fechaNacimiento;
}
direccion_t cliente::get_domicilio(){
  return this->domicilio;
}
telefono_t cliente::get_contacto(){
  return this->contacto;
}
//virtual ~cliente ();
