#ifndef CLIENTE_H
#define CLIENTE_H
#include <string>
//fecha_t
typedef struct {
  int dia;
  int mes;
  int anho;
}fecha_t;
//direccion_t
typedef struct {
        std::string calle;
        int numero;
        int cod_postal;
        std::string poblacion;
      }direccion_t;
//telefono_t
typedef struct {
        std::string casa;
        std::string oficina;
        std::string movil;
      }telefono_t;
//cliente
class cliente {
private:
  std::string nombre;
  std::string apellido_paterno;
  std::string apellido_materno;
  fecha_t fechaNacimiento;
  direccion_t domicilio;
  telefono_t contacto;

public:
  cliente();
  cliente (std::string,std::string,std::string,fecha_t, direccion_t,telefono_t );
  void set_nombre(const std::string);
  void set_apellido_paterno(std::string);
  void set_apellido_materno(std::string);
  void set_fechaNacimiento(int, int, int);
  void set_fechaNacimiento(fecha_t );
  void set_domicilio(std::string, int, int, std::string );
  void set_contacto(std::string, std::string, std::string);
  std::string get_nombre();
  std::string get_apellido_paterno();
  std::string get_apellido_materno();
  fecha_t get_fechaNacimiento();
  direccion_t get_domicilio();
  telefono_t get_contacto();
  //virtual ~cliente ();
};
#endif
