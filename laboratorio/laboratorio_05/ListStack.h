#include "StackADT.h"

struct node{
  int e;
  node *next;
};

class ListStack : public StackADT{
 private:
  struct node *_head;
  int _size;
 public:
  ListStack();
  ~ListStack();
  bool empty();
  int size();
  int top();
  void push(int);
  void pop();
};
