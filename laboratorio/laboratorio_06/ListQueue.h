#include "QueueADT.h"

struct node{
  int e;
  node *next;
};

class ListQueue : public QueueADT{
 private:
  struct node *_front;
  struct node *_back;
  int _size;
 public:
  ListQueue();
  ~ListQueue();
  bool empty();
  int size();
  int front();
  int back();
  void enqueue(int);
  void dequeue();
};
